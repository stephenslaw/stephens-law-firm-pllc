Stephens Law Firm, LLC is one of the top personal injury law firms in Fort Worth, TX. Founding attorney Jason Stephens has over 22 years of experience representing personal injury victims in Fort Worth and Texas. 

When it comes to results, its very simple. Jason has secured extraordinary results in the courtroom, which include some record-breaking verdicts. In 2004, Jason won the 5th largest verdict in Texas history and 47th largest ever in the country. In 2012, he won the 8th largest motor vehicle accident verdict ever awarded by a Texas jury. These verdicts are only two examples of Jasons unwavering commitment to fighting for his clients. 

These results and Jasons exceptional service to his clients have earned him numerous awards from some of the most esteemed legal organizations in the state. Some of the titles Jason has received are: Best Lawyers in America (2016-Present), Top 100 Trial Lawyer by the National Trial Lawyers Association (2011-present), Texas Super Lawyer by Thompson Reuters (2004-Present), Top 100 Super Lawyer in all of Dallas/Fort Worth by Thomas Reuters (2017, 2018), and more. He has also earned a perfect 10 rating from Avvo and Martindale-Hubbells prestigious AV Rating. 

Jason has earned all of his success by striving to be a difference maker. He understands that his clients have gone through traumatizing life events and may never know their old normal again. This is why he and his team pursue excellence in everything they do in order to provide trustworthy representation to their clients. 

At Stephens Law, we take your trust seriously. We want to make sure that you feel comfortable with the attorney you hire to represent you, which is why we offer a free, no-obligation consultation.

Call us today to see how we can help you. There is no obligation, and there is no fee unless we win. 

Stephens Law, PLLC

1300 South University Drive Suite 406
Fort Worth, TX 76107

(817) 420-7000

https://www.stephenslaw.com/fort-worth/